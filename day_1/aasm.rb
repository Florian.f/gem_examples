class Account
  include AASM

  aasm do
    state :unconfirmed, initial: true
    state :confirmed
    state :deleted

    event :confirm do
      transitions from: :unconfirmed, to: :confirmed
    end

    event :delete do
      transitions from: [:unconfirmed, :confirmed], to: :deleted, success: :deleted_alert
    end
  end

  private 

  def deleted_alert
    # ...
  end
end


class Application
  account = Account.new

  account.confirmed?
  
  account.confirm

  account.confirmed?
end